package arkady.client;

import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.*;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ClientApplication {

    public static void main(String[] args) {
        WebSocketClient client = new StandardWebSocketClient();
        WebSocketTransport transport = new WebSocketTransport(client);
        List<Transport> transports = Collections.singletonList(transport);
        SockJsClient sockJsClient = new SockJsClient(transports);
        WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());

        String url = "ws://localhost:8080/message";
//        String clientId = args[0];
        String clientId = "client_1";

        StompSessionHandler handler = new KpStompSessionHandler(clientId);
        try {
            StompSession session = stompClient.connect(url, handler).get();

            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                System.out.print(clientId + " >> ");
                System.out.flush();
                String line = in.readLine();
                if (line.length() == 0) continue;

                ClientMessage msg = new ClientMessage(clientId, line);
                session.send("/app/message/" + clientId, msg);
            }
        } catch (InterruptedException | ExecutionException | IOException e) {
            e.printStackTrace();
        }
    }

    static public class KpStompSessionHandler extends StompSessionHandlerAdapter {
        private String clientId;

        public KpStompSessionHandler(String clientId) {
            this.clientId = clientId;
        }

        @Override
        public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
            System.out.println("Connected to server!");
            subscribeTopic("/client/" + clientId, session);
            sendJsonMessage(session);
        }

        private void subscribeTopic(String topic, StompSession session) {
            session.subscribe(topic, new StompFrameHandler() {

                @Override
                public Type getPayloadType(StompHeaders headers) {
                    return ServerMessage.class;
                }

                @Override
                public void handleFrame(StompHeaders headers, Object payload) {
                    ServerMessage message = (ServerMessage) payload;
                    System.out.println("server >> " + message.getText());
                }
            });
        }

        private void sendJsonMessage(StompSession session) {
            ClientMessage clientMessage = new ClientMessage(clientId, "test");
            session.send("/app/message/" + clientId, clientMessage);
        }
    }
}
